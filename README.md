# module.ecs-service

## Provider Requirements:
* **aws:** (any version)

## Input Variables
* `azs` (required): List of availability zones to use with ECS
* `cluster` (required): Name of the ECS cluster to use
* `container_definitions` (default `"[]"`): JSON container task definition
* `custom_policies` (required): Custom statements related to task role
* `desired_count` (default `1`): Desired count of running tasks
* `mount_host_path` (required): ECS task definition - volume mount host path
* `name` (required): Name of the ECS service
* `project_and_env` (required): Name of the project and environment
* `region` (required): Region to run the ECS in
* `scheduling_strategy` (default `"REPLICA"`): Scheduling strategy of the service
* `tgs_arn_port` (required): Object of ARNs of the target groups with their ports
* `volume_name` (required): Name of the ECS acme volume

## Output Values
* `task_role`

## Managed Resources
* `aws_ecs_service.service` from `aws`
* `aws_ecs_task_definition.service` from `aws`
* `aws_iam_role.execution` from `aws`
* `aws_iam_role.role` from `aws`
* `aws_iam_role.task` from `aws`
* `aws_iam_role_policy.log_agent` from `aws`
* `aws_iam_role_policy.task_execution` from `aws`

## Data Resources
* `data.aws_iam_policy_document.assume-role-policy` from `aws`
* `data.aws_iam_policy_document.task_assume` from `aws`
* `data.aws_iam_policy_document.task_execution_permissions` from `aws`
* `data.aws_iam_policy_document.task_role_permissions` from `aws`


