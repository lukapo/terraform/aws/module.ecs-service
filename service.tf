resource "aws_ecs_service" "service" {
  name                = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.name}"
  cluster             = var.cluster
  task_definition     = aws_ecs_task_definition.service.arn
  scheduling_strategy = var.scheduling_strategy
  desired_count       = var.desired_count

  dynamic "load_balancer" {
    for_each = var.tgs_arn_port
    content {
      target_group_arn = load_balancer.value.target_group_arn
      container_name   = load_balancer.value.container_name
      container_port   = load_balancer.value.container_port
    }
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${join(",", var.azs)}]"
  }
}
