variable "region" {
  description = "Region to run the ECS in"
  type        = string
}

variable "name" {
  description = "Name of the ECS service"
  type        = string
}

variable "project_and_env" {
  description = "Name of the project and environment"
  type        = string
  default     = ""
}

variable "cluster" {
  description = "Name of the ECS cluster to use"
  type        = string
}

variable "azs" {
  description = "List of availability zones to use with ECS"
  type        = list(string)
}

variable "tgs_arn_port" {
  description = "Object of ARNs of the target groups with their ports"
  type = list(object({
    container_name   = string
    container_port   = number
    target_group_arn = string
  }))
  default = []
}

variable "volume_name" {
  description = "Name of the ECS acme volume"
  type        = string
}

variable "mount_host_path" {
  description = "ECS task definition - volume mount host path"
  type        = string
}

variable "scheduling_strategy" {
  description = "Scheduling strategy of the service"
  type        = string
  default     = "REPLICA"
}

variable "container_definitions" {
  description = "JSON container task definition"
  type        = string
  default     = "[]"
}

variable "desired_count" {
  description = "Desired count of running tasks"
  default     = 1
  type        = number
}

variable "custom_policies" {
  description = "Custom statements related to task role"
  type = list(object({
    effect    = string
    actions   = list(string)
    resources = list(string)
  }))
  default = []
}
