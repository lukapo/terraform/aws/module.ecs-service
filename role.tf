data "aws_iam_policy_document" "assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "role" {
  name               = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.name}_role"
  assume_role_policy = data.aws_iam_policy_document.assume-role-policy.json
}

resource "aws_iam_role_policy" "log_agent" {
  name   = "traefik-task-permissions"
  role   = aws_iam_role.task.id
  policy = data.aws_iam_policy_document.task_role_permissions.json
}

resource "aws_iam_role" "task" {
  name               = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.name}-task-role"
  assume_role_policy = data.aws_iam_policy_document.task_assume.json
}

data "aws_iam_policy_document" "task_assume" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "task_execution_permissions" {
  statement {
    effect = "Allow"

    resources = [
      "*",
    ]

    actions = [
      "ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage",
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams"
    ]
  }
}

resource "aws_iam_role_policy" "task_execution" {
  name   = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.name}_execution_role_policy"
  role   = aws_iam_role.execution.id
  policy = data.aws_iam_policy_document.task_execution_permissions.json
}

resource "aws_iam_role" "execution" {
  name               = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.name}_execution_role"
  assume_role_policy = data.aws_iam_policy_document.task_assume.json
}
