resource "aws_ecs_task_definition" "service" {
  family                = "${var.project_and_env == "" ? "" : "${var.project_and_env}-"}${var.name}"
  container_definitions = var.container_definitions
  execution_role_arn    = aws_iam_role.execution.arn
  task_role_arn         = aws_iam_role.task.arn

  volume {
    name      = var.volume_name
    host_path = var.mount_host_path
  }
}
